from psnn import NeuralNetwork, HiddenLayer
import os
import sys
from pathlib import Path
import torch
from torchvision import datasets, transforms
from torch.autograd import Variable
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use("GTK3Agg")

# Set up the MNIST data set.
kwargs = {}
train_data = torch.utils.data.DataLoader(datasets.MNIST('data',
                                                    train=True,
                                                    download=True,
                                                    transform=transforms.Compose([
                                                        transforms.ToTensor(),
                                                        transforms.Normalize((0.1307,),(0.3081,))
                                                    ])
                                                    ),
                                    batch_size=64,
                                    shuffle=True,
                                    **kwargs
                                    )
test_data = torch.utils.data.DataLoader(datasets.MNIST('data',
                                                    train=False,
                                                    transform=transforms.Compose([
                                                        transforms.ToTensor(),
                                                        transforms.Normalize((0.1307,),(0.3081,))
                                                    ])
                                                    ),
                                    batch_size=64,
                                    shuffle=True,
                                    **kwargs
                                    )

skip_train = False
if len(sys.argv) == 2:
    if sys.argv[1] == "skip":
        # skip training
        skip_train = True

# Try to load the last saved state.
if Path("network/netw0").is_file():
    network = NeuralNetwork.load("netw0", "network")
else:
    network = NeuralNetwork()
    # 28*28 inputs (28x28 pictures in the dataset) and 10 outputs with 6 layers.
    network.init(28*28, 6, [28*28, 28*14, 28*7, 28*3, 28, 10])

if not skip_train:
    # For logging / plotting
    error_rates = []
    t = np.zeros((10,1),dtype=float)
    for batch_id, (data, target) in enumerate(train_data):
        error_rate = 0.0
        for i in range(len(data)):
            # Convert the MNIST format to the format we use.
            t = np.zeros((10, 1), dtype=float)
            dat = np.array(data[i][0])
            t[target[i].item()][0] = 1
            dat2 = np.zeros((28*28,1),dtype=float)
            for i in range(28):
                for j in range(28):
                    dat2[i*28+j][0] = dat[i][j]
            dat = dat2
            # Train the network with this single picture
            error_rate += network.train(dat, t, 0.01)
        # Append the error rate of this whole batch
        error_rates.append(np.sum(error_rate)/10)
        print("Batch #{}: {}".format(batch_id, np.sum(error_rate)/10))

        # Save every 100 batches
        if (batch_id+1) % 100 == 0:
            network.save("netw0", "network")

    plt.plot(error_rates)
    plt.ylabel('Sum of all error rates')
    plt.xlabel('#Batch')
    plt.show()

print("STARTING TESTING PROCESS")

error_rates = []
t = np.zeros((10,1),dtype=float)
for batch_id, (data, target) in enumerate(test_data):
    error_rate = 0.0
    for i in range(len(data)):
        t = np.zeros((10, 1), dtype=float)
        dat = np.array(data[i][0])
        t[target[i].item()][0] = 1
        dat2 = np.zeros((28*28,1),dtype=float)
        for i in range(28):
            for j in range(28):
                dat2[i*28+j][0] = dat[i][j]
        dat = dat2
        error_rate += (t - network.think(dat))
    error_rates.append(np.sum(error_rate)/10)
    print("Batch #{}: {}".format(batch_id, np.sum(error_rate)/10))

plt.plot(error_rates)
plt.ylabel('Sum of all error rates')
plt.xlabel('#Batch')
plt.show()