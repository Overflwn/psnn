"""
    Python Simple Neural Network

A very very simple neural network made with just numpy and raw python. (object-oriented)
Additionally to that this code uses the network to train and test it on the MNIST dataset.

~Overflwn (2020)
"""

import random
import json
import os
import sys
from pathlib import Path
import numpy as np
import torch
from torchvision import datasets, transforms
from torch.autograd import Variable


# matrix of inputs (3x1 matrix)
# [
#     [ i1 ],
#     [ i2 ],
#     [ i3 ]
# ]

# weights of 2 nodes (one row = one node, column = weight)
# [
#     [ w11, w12, w13 ],
#     [ w21, w22, w23 ],
# ]

# the output of weights * inputs
# [
#     [ w11 * i1 + w12 * i2 + w13 * i3 ],
#     [ w21 * i1 + w22 * i2 + w23 * i3 ]
# ]



class NeuralNetwork:
    """The main class.
    This class holds objects of hidden layers and initializes them
    based on how many inputs the network should have, how many layers
    there are supposed to be and how many nodes each layer should have.
    """
    def __init__(self):
        """Standard Constructor

        Use init(...) or from_data(...) afterwards.
        """
        self.input_nodes = 0
        self.hidden = []

    def init(self, num_in: int, num_hidden: int, num_nodes: list):
        """Initialization method.
        Creates a new model based on the number of inputs, number of hidden layers
        and an array containing the number of nodes for each hidden layer. The last
        hidden layer is also the "output" layer, the network gives as many output values
        as there are nodes in the last hidden layer.
        Example:
        init(3, 2, [5, 3])
        -> Network with 3 input values, 2 hidden layers with the first having 5 nodes and
        the last having 3 nodes which also means that there are 3 outputs from the network.
        """
        self.input_nodes = num_in
        self.hidden = []
        last_outputs = -1
        # Create each layer and connect the outputs to the inputs from the next layer.
        for i in range(num_hidden):
            if i == 0:
                inputs = num_in
            else:
                inputs = last_outputs
            hid = HiddenLayer()
            hid.init(inputs, num_nodes[i])
            last_outputs = num_nodes[i]
            self.hidden.append(hid)

    def from_data(self, num_in: int, hidden: list):
        """Load the data from given variables."""
        self.input_nodes = num_in
        self.hidden = hidden

    @staticmethod
    def sigmoid(data):
        """The activation function."""
        return 1 / (1 + np.exp(-data))

    @staticmethod
    def der_sigmoid(data):
        """The derivative of the activation function.
        Used in the backpropagation."""
        return NeuralNetwork.sigmoid(data) * (1 - NeuralNetwork.sigmoid(data))

    def save(self, path: str, folder: str):
        """Save each layer and store the number of hidden layers and the number of inputs."""
        for i in range(len(self.hidden)):
            self.hidden[i].save("{}/{}".format(folder, i))
        with open("{}/{}".format(folder, path), "w") as file:
            file.write("{}\n".format(self.input_nodes))
            file.write("{}".format(len(self.hidden)))

    @staticmethod
    def load(path: str, folder: str):
        """Return a network containing the data that was previously saved."""
        num_hid = 0
        num_in = 0
        with open("{}/{}".format(folder, path), "r") as file:
            num_in = int(file.readline())
            num_hid = int(file.readline())
        hidden = []
        for i in range(num_hid):
            hidden.append(HiddenLayer.load("{}/{}".format(folder, i)))

        neural_net = NeuralNetwork()
        neural_net.from_data(num_in, hidden)
        return neural_net

    def think(self, data):
        """Calculate the output of the data throughout the whole network.

        Args:
            data (ndarray): The values the network should think about
                            Note: The data has to be a vector containing exactly the same amount of
                            values as there are inputs.

        Returns:
            ndarray: The output value for each value of the input array.
        """
        output = data
        for i in range(0, len(self.hidden)):
            output = NeuralNetwork.sigmoid(self.hidden[i].forward(output))
        return output

    def train(self, data, answers, learning_rate: float):
        """Calculate the error based on the output and the target output.
        Then reverse-propagade to adjust every weight and bias based on the
        learning rate.
        """
        # error = targets - outputs
        outputs = self.think(data)
        error = answers - outputs
        orig_err = error
        if len(self.hidden) >= 2:
            previous_input = NeuralNetwork.sigmoid(self.hidden[len(self.hidden)-2].output)
        else:
            previous_input = data
        self.hidden[len(self.hidden)-1].fix(previous_input, error, learning_rate)
        weights_t = self.hidden[len(self.hidden)-1].weights.T
        for i in range(len(self.hidden)-2, -1, -1):
            if i > 0:
                previous_input = NeuralNetwork.sigmoid(self.hidden[i - 1].output)
            else:
                previous_input = data
            error = np.dot(weights_t, error)
            self.hidden[i].fix(previous_input, error, learning_rate)
            weights_t = self.hidden[i].weights.T
        return orig_err


class HiddenLayer:
    """Probably the 'big' part of the code. It defines a hidden layer.
    It holds a matrix of weights (shape := num_nodes x num_inputs) and a vector
    of biases (shape := num_nodes x 1).
    It has the forward function (w/o activation function) and the fix function,
    which basically just adjusts the weights and biases based on the errors that
    every single node had based on the errors from the next layer.
    """
    def __init__(self):
        """Standard Constructor
        Use init(...) or from_data(...) afterwards.
        """
        self.weights = None
        self.biases = None
        self.output = None

    def init(self, num_inputs: int, num_neurons: int):
        """Initialization method.
        Creates a new layer based on the number of inputs and the number of  nodes (neurons)
        this layer should have.
        Example:
        init(3, 2)
        -> Layer with 3 input values and 2 nodes/neurons -> 2 outputs.
        """
        self.weights = []
        self.biases = []
        self.output = np.zeros((num_neurons, 1))
        for _i in range(num_neurons):
            new_weights = []
            for _j in range(num_inputs):
                new_weights.append(np.random.random())
            self.weights.append(np.array(new_weights))
            self.biases.append([np.random.random()])
        self.weights = np.array(self.weights)

    def from_data(self, weights, biases, output):
        """Load the data from given variables."""
        self.weights = weights
        self.biases = biases
        self.output = output

    def forward(self, inputs):
        """The forward function calculates the output without activation function."""
        self.output = np.dot(self.weights, inputs) + self.biases
        return self.output

    def fix(self, inputs, errors, learning_rate: float):
        """Adjust the node's weights and biases based on the error percentage and learning rate."""
        # Algorithm basically "taken" from the CodingTrain series on neural networks.
        dt_errors = learning_rate * errors
        delta = dt_errors * NeuralNetwork.der_sigmoid(self.output) * inputs.T
        delta_b = dt_errors
        self.weights += delta
        self.biases += delta_b

    def save(self, path: str):
        """Save the node's data onto seperate files."""
        np.save("{}_weights.lay".format(path), self.weights)
        np.save("{}_biases.lay".format(path), self.biases)
        np.save("{}_output.lay".format(path), self.output)

    @staticmethod
    def load(path: str):
        """Load the weights, biases and last output from files
        and create a HiddenLayer with these values."""
        hidden_layer = HiddenLayer()
        hidden_layer.from_data(
            np.load("{}_weights.lay.npy".format(path)),
            np.load("{}_biases.lay.npy".format(path)),
            np.load("{}_output.lay.npy".format(path)))
        return hidden_layer
        